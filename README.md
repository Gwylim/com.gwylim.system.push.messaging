# STOMP WebSockets Typed Subscription framework
### Background
STOMP is a pub/sub layer over web sockets, clients can subscribe to destinations and will receive all messages sent 
there.  
This is very useful, for example, updating all members of a game about the events in that game. 
It would be really useful to not have to do serialisation or routing of events to the correct destination for each 
type of event that can happen.

### Goals
- Routing of messages to destinations should be easy to specify and require minimal code.
- We can have multiple different instances of each destination type, for example, different games, we must be able to
 publish and subscribe to a specific one.
- We must be able to restrict the destination to which a client can subscribe, e.g, only the players in a game can 
subscribe to moves in that game occurring in that game.
- Interaction with the serve must be simple for other languages for ether subscription or receipt of messages. 

### Implementation
Unsurprisingly the code is client and server:  
- **com.gwylim.system.push.messaging.server.PushServer**  
Used by the server to send messages to destinations.  
- **com.gwylim.system.push.messaging.client.PushClient**  
Used by clients to subscribe to destinations. This allows types to be shared between server and client for 
destination specification and message de-serialisation.

This implementation is inspired by feign, but instead of resources there are message types which are annotated to 
specify the destination they are sent to and specify the identifiers for different instances. 
@SubscriptionPath indicates the destination that the type will be sent to and the interface SubscribeBy allows the 
returning of a set of identifiers that will be inserted into the path provided by the @SubscriptionPath annotation.  

The client can then use the same types passed to the PushClient to create a subscription. If the type in question 
requires an identifier this can also be passed in. Clients subscribe with a consumer for the event type, the returned 
Subscription object can be used to manage the subscription, in particular "unsubscribe".  

The server is simple to interact with, one can look for the types that define messages and look for the path on the 
@SubscriptionPath annotation and the identifier it needs given the SubscribeBy interface, the received text will be 
JSON of the structure indicated by the type.