package com.gwylim.system.push.messaging.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gwylim.common.models.Reference;

import java.util.Set;

public interface SubscribeBy<T extends Reference> {
    @JsonIgnore
    Set<T> getSubscriptionIdentifiers();
}
