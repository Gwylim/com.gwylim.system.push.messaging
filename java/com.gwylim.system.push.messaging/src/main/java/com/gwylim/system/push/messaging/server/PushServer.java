package com.gwylim.system.push.messaging.server;

import com.gwylim.common.ReflectionUtil;
import com.gwylim.common.models.Reference;
import com.gwylim.system.push.messaging.common.SubscribeBy;
import com.gwylim.system.push.messaging.common.SubscriptionPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.core.MessageSendingOperations;

import java.util.Set;

public class PushServer {
    private static final Logger LOG = LoggerFactory.getLogger(PushServer.class);

    private final MessageSendingOperations<String> messagingTemplate;

    public PushServer(final MessageSendingOperations<String> messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }


    private void send(final String destination, final Object payload) {
        LOG.info("Send a {} to {}.", payload.getClass().getSimpleName(), destination);
        messagingTemplate.convertAndSend(destination, payload);
    }

    public void send(final Object payload) {
        final Class<?> messageClass = payload.getClass();

        final SubscriptionPath annotation = ReflectionUtil.getAnnotation(messageClass, SubscriptionPath.class)
                                                          .orElseThrow(() -> new IllegalArgumentException(messageClass.getName() + " does not have @SubscriptionPath so cannot be auto-subscribed."));

        final String destination = annotation.value();

        if (payload instanceof SubscribeBy) {
            if (!destination.contains("{id}")) {
                throw new IllegalArgumentException(messageClass.getName() + " @SubscriptionPath contains ID but class is not instanceof SubscribeBy.");
            }

            final Set<? extends Reference> references = ((SubscribeBy<?>) payload).getSubscriptionIdentifiers();

            references.forEach(reference -> {
                final String destinationWithRef = destination.replace("{id}", reference.getValue());
                send(destinationWithRef, payload);
            });
        }
        else {
            if (destination.contains("{id}")) {
                throw new IllegalArgumentException(messageClass.getName() + " @SubscriptionPath contains ID but class is not instanceof SubscribeBy.");
            }
            send(destination, payload);
        }
    }
}
