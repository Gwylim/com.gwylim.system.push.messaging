package com.gwylim.system.push.messaging.client;

import javax.annotation.CheckReturnValue;

import com.gwylim.common.ReflectionUtil;
import com.gwylim.common.ThreadUtil;
import com.gwylim.common.models.Reference;
import com.gwylim.system.push.messaging.common.SubscribeBy;
import com.gwylim.system.push.messaging.common.SubscriptionPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;

import java.lang.reflect.Type;
import java.util.function.Consumer;

import static com.gwylim.common.ReflectionUtil.getGenericClass;

public class PushClient {
    private static final Logger LOG = LoggerFactory.getLogger(PushClient.class);

    private final PushHeaderProvider headerProvider;

    private final StompSession session;

    PushClient(final StompSession session, final PushHeaderProvider headerProvider) {
        this.session = session;
        this.headerProvider = headerProvider;
    }

    @CheckReturnValue
    private <T> Subscription subscribe(final String destination, final Class<T> type, final Consumer<T> handler) {
        LOG.info("Subscribe to " + destination + " with type: " + type.getSimpleName());

        final PushClientStompFrameHandler<T> frameHandler = new PushClientStompFrameHandler<>(type, handler);

        final StompHeaders stompHeaders = new StompHeaders();
        stompHeaders.setDestination(destination);
        stompHeaders.setAll(headerProvider.get());

        return new Subscription(destination, session.subscribe(stompHeaders, frameHandler));
    }


    /**
     * Auto-subscribe based on the generic type of the handler.
     * Handled type must be annotated with @SubscriptionPath.
     *
     * If @SubscriptionPath contains {id} then the subscribe(Consumer<T>, ? extends Reference) must be used.
     */
    @CheckReturnValue
    public <T> Subscription subscribe(final Consumer<T> handler) {
        final Class<T> messageClass = getMessageClass(handler);
        final SubscriptionPath annotation = getSubscriptionPathAnnotation(messageClass);

        if (messageClass.isAssignableFrom(SubscribeBy.class)) {
            throw new IllegalArgumentException(messageClass.getName() + " is SubscribeBy so subscription Reference must be provided.");
        }

        final String destination = annotation.value();

        if (destination.contains("{id}")) {
            throw new IllegalArgumentException(messageClass.getName() + " @SubscriptionPath contains ID but class is not instanceof SubscribeBy.");
        }

        return subscribe(destination, messageClass, handler);
    }

    /**
     * Auto-subscribe based on the generic type of the handler.
     * Handled type must be annotated with @SubscriptionPath.
     *
     * Handled type must implement SubscribeBy so it can be sent to the right topic.
     */
    @CheckReturnValue
    public <R extends Reference, T extends SubscribeBy<R>> Subscription subscribe(final R reference, final Consumer<T> handler) {
        final Class<T> messageClass = getMessageClass(handler);
        final SubscriptionPath annotation = getSubscriptionPathAnnotation(messageClass);

        final String destination = annotation.value();

        if (!destination.contains("{id}")) {
            throw new IllegalArgumentException(messageClass.getName() + " @SubscriptionPath contains ID but class is not instanceof SubscribeBy.");
        }

        final String destinationWithRef = destination.replace("{id}", reference.getValue());

        return subscribe(destinationWithRef, messageClass, handler);
    }

    /**
     * A handle for a subscription so that it can be unsubscribed from.
     */
    public static final class Subscription {
        private final String destination;
        private final StompSession.Subscription subscription;

        private boolean unsubscribed = false;

        private Subscription(final String destination, final StompSession.Subscription subscription) {
            this.destination = destination;
            this.subscription = subscription;
        }

        /**
         * Send unsubscribe so that messages will no-longer be sent from the server.
         */
        public synchronized void unsubscribe() {
            if (!unsubscribed) {
                LOG.info("Unsubscribed from {}.", destination);
                subscription.unsubscribe();
                unsubscribed = true;
            }
            else {
                LOG.warn("Subscription unsubscribed more than once. " + destination);
            }
        }

        /**
         * If the subscription goes out of scope assume that we should unsuscribe.
         * This should really be done manually so log a warning.
         */
        @Override
        @SuppressWarnings("deprecation")
        protected synchronized void finalize() {
            if (!unsubscribed) {
                LOG.warn("DANGER! Subscription finalised before unsubscribed HANDLER WILL NO-LONGER BE INVOKED. " + destination);
                unsubscribe();
            }
        }
    }

    private static final class PushClientStompFrameHandler<T> implements StompFrameHandler {
        private final Type type;
        private final Consumer<T> handler;

        private PushClientStompFrameHandler(final Class<T> type, final Consumer<T> handler) {
            this.type = type;
            this.handler = handler;
        }

        @Override
        @NonNull
        public Type getPayloadType(@Nullable final StompHeaders headers) {
            return type;
        }

        @Override
        @SuppressWarnings("unchecked")
        public void handleFrame(@Nullable final StompHeaders headers, final Object payload) {
            ThreadUtil.run(() -> handler.accept((T) payload));
        }
    }



    private <T> SubscriptionPath getSubscriptionPathAnnotation(final Class<T> messageClass) {
        return ReflectionUtil.getAnnotation(messageClass, SubscriptionPath.class)
                .orElseThrow(() -> new IllegalArgumentException(messageClass.getName() + " does not have @SubscriptionPath so cannot be auto-subscribed."));
    }

    @SuppressWarnings("unchecked")
    private <T> Class<T> getMessageClass(final Consumer<T> handler) {
        final Class<T> messageClass = (Class<T>) getGenericClass(handler.getClass(), Consumer.class);
        if (messageClass == null) {
            throw new IllegalArgumentException("Could not divine message type for " + handler.getClass().getName());
        }
        return messageClass;
    }
}
