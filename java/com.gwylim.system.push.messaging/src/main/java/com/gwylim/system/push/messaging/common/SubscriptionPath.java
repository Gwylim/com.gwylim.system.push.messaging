package com.gwylim.system.push.messaging.common;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface SubscriptionPath {

    /**
     * The path to which one subscribes to get updates on this event.
     * If the Annotated class extends SubscribeBy then {id} must appear in the path.
     */
    String value();
}
