package com.gwylim.system.push.messaging.client;

import javax.annotation.Nonnull;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;

/**
 * Extend to provide PushClientFactory.
 */
public abstract class PushClientFactoryBase {
    private static final Logger LOG = LoggerFactory.getLogger(PushClientFactoryBase.class);

    private final String url;

    public PushClientFactoryBase(@Value("${com.gwylim.games.web.tictac.api.websocket}") final String url) {
        this.url = url;
    }

    public PushClient createClient(final PushHeaderProvider headerProvider) {
        LOG.info("Connecting to websocket: " + url + " with headers: " + headerProvider.get());

        final WebSocketClient webSocketClient = new StandardWebSocketClient();
        final WebSocketStompClient stompClient = new WebSocketStompClient(webSocketClient);

        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
        stompClient.setTaskScheduler(new ConcurrentTaskScheduler());

        LOG.info(headerProvider.get().toString());
        final StompHeaders headers = new StompHeaders();
        headerProvider.get().forEach(headers::add);

        try {
            final StompSession session = stompClient.connect(url, new WebSocketHttpHeaders(), headers, new STOMPSessionHandler()).get();
            LOG.info("Connect stomp session SUCCESS.");

            return new PushClient(session, headerProvider);
        }
        catch (final InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    static class STOMPSessionHandler extends StompSessionHandlerAdapter {
        @Override
        public void afterConnected(final StompSession session, @Nonnull final StompHeaders connectedHeaders) {
            LOG.info("New session: {}", session.getSessionId());
        }

        @Override
        public void handleTransportError(final StompSession session, @Nonnull final Throwable exception) {
            LOG.error("Stomp transport error for session: {}", session.getSessionId());
        }
    }
}
