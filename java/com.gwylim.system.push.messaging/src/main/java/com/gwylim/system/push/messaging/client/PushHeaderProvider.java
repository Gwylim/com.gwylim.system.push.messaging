package com.gwylim.system.push.messaging.client;

import java.util.Map;
import java.util.function.Supplier;

public interface PushHeaderProvider extends Supplier<Map<String, String>> {
}
