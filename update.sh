#!/bin/bash
set -e

TIMESTAMP=`date -u -I`
JAR="com.gwylim.games.web.tictac.ui.vaadin"
LOG_FILE="${JAR}-${TIMESTAMP}.log"

# Environmental variables:
TICTAC_HOST=${TICTAC_HOST}
TICTAC_HOST_USER=${TICTAC_HOST_USER}

if [ -z "${TICTAC_HOST}" ]
then
    echo TICTAC_HOST environmantal variable not set.
fi

if [ -z "${TICTAC_HOST_USER}" ]
then
    echo TICTAC_HOST_USER environmantal variable not set.
fi

USER=${TICTAC_HOST_USER}
HOST=${TICTAC_HOST}
SSH="${USER}@${HOST}"

echo Building...
./gradlew clean build

echo Copy new jar...
scp java/${JAR}/build/libs/${JAR}.jar ${SSH}:

echo Kill running process...
PID=`ssh ${SSH} "ps ux" | grep ${JAR}.jar | grep -v grep | awk '{ print $2 }'`

if [ -z "${PID}" ]
then
    echo Tictac not running remotely.
else    
    echo Killing PID: ${PID}...
    ssh ${SSH} "kill ${PID}"

    echo Wait for it to die...
    sleep 10s
fi

echo Start new process...
# TODO: Set up logging.
ssh ${SSH} "nohup java -jar ${JAR}.jar > ${LOG_FILE} &"

echo What does the log, ${LOG_FILE} say?
ssh ${SSH} "tail -F ${LOG_FILE}"
